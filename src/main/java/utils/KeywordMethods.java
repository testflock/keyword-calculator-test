package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.nio.file.Paths;

public class KeywordMethods {

    static WebDriver driver;
    static WebDriverWait wait;

    public void open_browser(String browserName) {
        try {
            if (browserName.equalsIgnoreCase("firefox")) {
                String filePathString = Paths.get(".").toAbsolutePath().normalize().toString()
                        + "/src/main/resources/drivers/geckodriver_mac";
                System.setProperty("webdriver.gecko.driver", filePathString);
                WebDriver driver = new FirefoxDriver();

                driver.manage().window().maximize();
            } else if (browserName.equalsIgnoreCase("chrome")) {
                String filePathString = Paths.get(".").toAbsolutePath().normalize().toString()
                        + "/src/main/resources/drivers/chromedriver_mac";
                System.setProperty("webdriver.chrome.driver", filePathString);
                driver = new ChromeDriver();
                focusBrowser(driver);
                driver.manage().window().maximize();
            }
        } catch (WebDriverException e) {
            System.out.println(e.getMessage());
        }
    }

    private void focusBrowser(WebDriver driver) {
        String currentWindowHandle;
        currentWindowHandle = driver.getWindowHandle();

        ((JavascriptExecutor)driver).executeScript("alert('testFlock.org')");
        driver.switchTo().alert().accept();

        driver.switchTo().window(currentWindowHandle);

    }

    public void enter_url(String URL) {
        driver.get(URL);
    }

    public By locatorValue(String locatorTpye, String value) {
        By by;
        switch (locatorTpye) {
            case "id":
                by = By.id(value);
                break;
            case "name":
                by = By.name(value);
                break;
            case "xpath":
                by = By.xpath(value);
                break;
            case "css":
                by = By.cssSelector(value);
                break;
            case "linkText":
                by = By.linkText(value);
                break;
            case "partialLinkText":
                by = By.partialLinkText(value);
                break;
            default:
                by = null;
                break;
        }
        return by;
    }

    public void enter_Text(String locatorType, String value, String text) {
        try {
            By locator;
            locator = locatorValue(locatorType, value);
            WebElement element = driver.findElement(locator);
            element.sendKeys(text);
        } catch (NoSuchElementException e) {
            System.err.format("No Element Found to enter text" + e);
        }
    }

    public void click_button(String locatorType, String value) {
        try {
            By locator;
            locator = locatorValue(locatorType, value);
            WebElement element = driver.findElement(locator);
            element.click();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (NoSuchElementException e) {
            System.err.format("No Element Found to perform click" + e);
        }
    }

    public void close_browser() {
        driver.quit();
    }

    public void select_standard_calculator() {
        String selectedView = driver
                .findElement(By.id("selectCalculator"))
                .getAttribute("checked");

        if (selectedView == "true") {
            driver.findElement(By.id("switch")).click();
        }

    }

    public void check_result(String locatorType, String value, String text) {
        try {
            By locator;
            locator = locatorValue(locatorType, value);
            WebElement element = driver.findElement(locator);
            Assert.assertTrue(element.getText().toString().equals(text));
        } catch (NoSuchElementException e) {
            System.err.format("No Element Found to enter text" + e);
        }

    }

    public void select_scientific_calculator() {
        String selectedView = driver
                .findElement(By.id("selectCalculator"))
                .getAttribute("checked");

        if (selectedView != "true") {
            driver.findElement(By.id("switch")).click();
        }

    }



}
