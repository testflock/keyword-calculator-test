# keyword-calculator-test

Sample Calculator Testing Program implemented with Java, Selenium, TestNG with a Keyword Driven Testing Framework.

demo page: http://www.testflock.org/calculator/

## Getting Started


## Authors

Vasilis Petrou | Automation Test Engineer
petrou82@gmail.com


## Acknowledgments

what is keyword driven testing framework?

keyword driven testing is an application independent framework utilising data tables and self explanatory keywords to explain the actions to be performed on the application under test.

not only is the test data kept in the file but even the directives called keywords.
